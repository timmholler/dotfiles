set nocompatible

"pluginmanagement

call plug#begin('~/.vim/plugged')

Plug 'lifepillar/vim-mucomplete'
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'tpope/vim-commentary'
"Plug 'godlygeek/tabular'
"Plug 'vim-syntastic/syntastic'
"Plug '~/.vim/plugged/vim-peekaboo'
"Plug 'itchyny/vim-gitbranch'
Plug 'junegunn/limelight.vim'
"Plug 'milkypostman/vim-togglelist'
Plug 'junegunn/goyo.vim'
Plug 'airblade/vim-gitgutter'
"Plug 'francoiscabrol/ranger.vim'
Plug 'lervag/vimtex'
Plug 'davidhalter/jedi-vim'
Plug 'majutsushi/tagbar'
"Plug 'ludovicchabant/vim-gutentags'
Plug 'mbbill/undotree'
Plug 'fxn/vim-monochrome'
Plug 'owickstrom/vim-colors-paramount'
call plug#end()

" settings

"plugin settings

"plugins I used to use
""""""""netrw
" let g:netrw_liststyle = 2 "details
" let g:netrw_sizestyle = "H" "human readable
" let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+' "hide dotfiles
" let g:netrw_hide = 1
" let g:netrw_banner = 0
" let g:netrw_winsize = 20
" autocmd FileType netrw setl bufhidden=delete
let g:netrw_browsex_viewer="xdg-open"
"http://google.de
""""""""nerdtree
" let g:NERDTreeNaturalSort=1
" let g:NERDTreeHighlightCursorline=1
" let g:NERDTreeShowBookmarks=1
" let g:NERDTreeWinSize=45
" let g:NERDTreeAutoDeleteBuffer=1
" let g:NERDTreeMinimalUI=1
" let g:NERDTreeQuitOnOpen=1
" let g:NERDTreeDirArrowExpandable=">"
" let g:NERDTreeDirArrowCollapsible="v"

"vim  ranger
let g:ranger_map_keys=0
"let g:ranger_replace_netrw=1

"undotree
let g:undotree_WindowLayout=2
let g:undotree_ShortIndicators=1
let g:undotree_SetFocusWhenToggle=1
let g:undotree_SplitWidth=40

"vim-togglelist
let g:toggle_list_no_mappings=1
"let g:loaded_netrwPlugin=0
"let g:loaded_netrw=0

"syntastic
let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "active_filetypes": ["python"],
    \ "passive_filetypes": [] }
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list=0
let g:syntastic_check_on_open=1
let g:syntastic_check_on_wq=0
let g:syntastic_quiet_messages= {"level": "warnings", "file:p":'&.tex$'}
let g:syntastic_quiet_messages= {"type": "style" }

"git gutter
let g:gitgutter_enabled=0
let g:gitgutter_map_keys = 0
let g:gitgutter_sign_added = 'A'
let g:gitgutter_sign_modified = 'M'
let g:gitgutter_sign_removed = 'D'
let g:gitgutter_override_sign_column_highlight = 0

"mucomplete:

"mandatory for mucomplete
set completeopt+=menuone
set completeopt+=noselect
set shortmess+=c "shuts off completion messages
set belloff+=ctrlg "for beeps
"settings
let g:mucomplete#enable_auto_at_startup = 1
"call add(g:mucomplete#chains['default'], 'ulti')
set omnifunc=syntaxcomplete#Complete
let g:mucomplete#chains={}
let g:mucomplete#chains.default = ["keyn", "c-n", 'path', 'omni', 'ulti']
let g:mucomplete#chains.vim = ["cmd", 'keyn', "c-n", 'path', 'omni', 'ulti']

"goyo + limelight
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave
autocmd User GoyoLeave colorscheme phoenixvar
autocmd User GoyoLeave Limelight!

let g:limelight_conceal_ctermfg='gray'
let g:limelight_conceal_ctermfg=240
let g:limelight_conceal_guifg='DarkGray'
let g:limelight_conceal_guifg='#777777'

"vimtex
"let g:vimtex_mappings_enabled=0
let g:vimtex_view_method='mupdf'
let g:vimtex_compiler_latexmk = {
	\ 'callback' : 0,
        \}
" 	\ 'continuous' : 1,
" 	\}
let g:vimtex_quickfix_latexlog = {
	\ 'default' : 0,
	\ 'overfull': 0,
	\ 'underfull' : 0,
	\ 'packages' : {
		\ 'default' : 0,
	\ },
	\}
let g:vimtex_quickfix_autojump = 1
let g:vimtex_quickfix_mode = 1

"ultisnips
let g:UltiSnipsSnippetDirectories=['/home/tmm/.vim/ultisnipsmine','/home/tmm/.vim/UltiSnips']
let g:UltiSnipsSnippetsDir='/home/tmm/.vim/ultisnipsmine'


"vim settings

"misc
set undofile
set ttimeout
set timeoutlen=350
syntax on
set textwidth=0
augroup plaineng
    autocmd!
    autocmd FileType tex setlocal textwidth=80
    autocmd BufNewFile,BufRead *.cls set filetype=tex
    autocmd FileType mail setlocal formatoptions+=aw
augroup END
let g:tex_comment_nospell=1
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab

set ssop-=options
set viewoptions-=options
au BufRead,BufNewFile bash-fc* set filetype=bashfc
let ftToIgnore = ['bashfc', 'qf', 'netrw', 'none']
augroup AutoSaveFolds
    autocmd!
    autocmd BufWinLeave * if index(ftToIgnore, &ft) <0 && !empty(&ft) | mkview | endif
    autocmd BufWinEnter * if index(ftToIgnore, &ft) <0 && !empty(&ft) | silent! loadview | endif
augroup END

set hidden
set viminfo+=n~/.vim/viminfo
filetype plugin indent on
set autoindent
set ruler
set wildmenu
set laststatus=2
set noshowmode
set fillchars=fold:·

"statusline
function! GitBranch()
    return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
    let l:branch = GitBranch()
    return strlen(l:branch) > 0?' '.l:branch.' ':''
endfunction

set noshowmode
set statusline=
set statusline+=%#TabLineFill#
set statusline+=[%{StatuslineGit()}]
set statusline+=[%f]%<
set statusline+=[%R]
"set statusline+=%{gutentags#statusline()}
set statusline+=[%m]
set statusline+=%=\[[%{&fileencoding?&filencoding:&encoding}]
set statusline+=[%l/%L]
set statusline+=]

"searching
set incsearch
set ignorecase
set smartcase
set hlsearch
highlight clear search

"whitespace
highlight BadWhite ctermbg=088
match BadWhite /\s\+$\| \+\ze\t\|\t\zs \+/
autocmd BufWinEnter * match BadWhite /\s\+$\| \+\ze\t\|\t\zs \+/
autocmd BufWinLeave * call clearmatches()

"cursorline
highlight clear CursorLine
set cursorline
highlight  CursorLine ctermbg=238 ctermfg=NONE

"foldtext
function! NeatFoldText()
    let line =''.substitute(getline(v:foldstart), '^\s*"\?\s*\|\s*"\?\s*{{' . '{\d*\s*', '', 'g') . ' '
    let lines_count = v:foldend - v:foldstart + 1
    let lines_count_text = '| ' . printf("%10s", lines_count . ' lines') . ' |'
    let foldchar = matchstr(&fillchars, 'fold:\zs.')
    let foldtextstart = strpart('' . repeat(foldchar, v:foldlevel*3-2) . '|' . line, 0, (winwidth(0)*2)/3)
    let foldtextend = lines_count_text . repeat(foldchar, 8)
    let foldtextlength = strlen(substitute(foldtextstart . foldtextend, '.', 'x', 'g')) + &foldcolumn
    return foldtextstart . repeat(foldchar, winwidth(0)-foldtextlength) . foldtextend
endfunction
set foldcolumn=0
set foldtext=NeatFoldText()

"nav
set number
"set colorcolumn=80
set splitbelow
set splitright
set number
set relativenumber
set scrolloff=20

"keybinds

"general
set backspace=indent,eol,start
nnoremap <SPACE> <Nop>
let mapleader = " "
nnoremap \sp :setlocal spell! spelllang=en_gb<CR>
nmap <Leader>\ :w<CR>
noremap<Leader><ESC> :q<CR>
inoremap <C-d> <Del>
inoremap jk <ESC>
nnoremap <F11> :mksession! ~/.vim/sessions/*.vim<C-D><BS><BS><BS><BS><BS>
nnoremap <F12> :source ~/.vim/sessions/*.vim<C-D><BS><BS><BS><BS><BS>

"word movement
nmap <Leader>t <Plug>GitGutterPrevHunk
nmap <Leader>y <Plug>GitGutterNextHunk

nnoremap <Leader>s gE
nnoremap <Leader>a B
nnoremap <Leader>f E
nnoremap <Leader>d W

vnoremap <Leader>s gE
vnoremap <Leader>a B
vnoremap <Leader>f E
vnoremap <Leader>d W

function! Newword(directionpos, visual)
    if a:visual
        normal! gv
    endif
    let c = v:count
    let i = 0
    while i<=c
        call search('\(\a\|\d\)\+',a:directionpos)
        let i +=1
    endwhile
endfunction

nnoremap <C-y> :<C-U>call Newword('bw', 0)<CR>
nnoremap <C-u> :<C-U>call Newword('ebw', 0)<CR>
nnoremap <C-i> :<C-U>call Newword('w', 0)<CR>
nnoremap <C-o> :<C-U>call Newword('ew', 0)<CR>

vnoremap <C-y> :<C-U>call Newword('bw', 1)<CR>
vnoremap <C-u> :<C-U>call Newword('ebw', 1)<CR>
vnoremap <C-i> :<C-U>call Newword('w', 1)<CR>
vnoremap <C-o> :<C-U>call Newword('ew', 1)<CR>

onoremap <C-y> :<C-U>call Newword('bw', 0)<CR>
onoremap <C-u> :<C-U>call Newword('ebw', 0)<CR>
onoremap <C-i> :<C-U>call Newword('w', 0)<CR>
onoremap <C-o> :<C-U>call Newword('ew', 0)<CR>


nnoremap <C-h> ^
nnoremap <C-j> }
nnoremap <C-k> {
nnoremap <C-l> $

vmap <C-l> $
vmap <C-h> ^
vmap <C-j> }
vmap <C-k> {
vmap <C-L> $

"window 
noremap <Leader>z za

noremap <Left> <c-w>H
noremap <Down> <c-w>J
noremap <Up> <c-w>K
noremap <Right> <c-w>L

noremap <Leader>h <c-w>h
noremap <Leader>j <c-w>j
noremap <Leader>k <c-w>k
noremap <Leader>l <c-w>l

noremap <Leader>r gt
noremap <Leader>q gT
noremap <Leader>w :bnext<CR>
noremap <Leader>e :bprev<CR>


function! FoldToggle()
    if &foldcolumn
        setlocal foldcolumn=0
    else
        setlocal foldcolumn=4
    endif
endfunction
nnoremap \f :call FoldToggle()<CR>


nnoremap <C-Up> :resize +2<cr>
nnoremap <C-Down> :resize -2<cr>
nnoremap <C-Left> :vertical resize +2<cr>
nnoremap <C-Right> :vertical resize -2<cr>

"copy/paste
nnoremap v <C-V>
nnoremap <C-V> v
xnoremap v <C-V>
xnoremap <C-V> v
nnoremap ' `
nnoremap gV `[v`]
vnoremap <C-X> <ESC>`.``gvP``P
noremap p gp
noremap P gP
noremap gp p
noremap gP P

"search
nnoremap n nzz
nnoremap N Nzz

nnoremap <silent> <BS> :nohlsearch<CR>
xnoremap // y/\V<C-r>=escape(@",'/\')<cr><cr>
nnoremap <Leader>N NzzgN
xnoremap <Leader>N NzzgN
xnoremap <Leader>n nzzgn
nnoremap <Leader>n nzzgn

function! GoogleSearch(vStr)
	silent! exec "silent! !luakit \"http://google.com/search?q=" . a:vStr . "\" &"
endfunction
xnoremap <Leader>? :call GoogleSearch(@*)<cr>:redraw!<cr><esc>
"minimize to visual selection
xnoremap <expr> <Leader>v 'z'.(2*(&scrolloff)+1+abs(line('.')-line('v')))."\<CR><ESC>".(min([line('.'),line('v')]))."ggzt"

"move through errors
function! s:qfnavi(Cmd1,Cmd2)
    try
    execute a:Cmd1
    catch /^Vim(\a\+):E553:/
    execute a:Cmd2
    endtry
    norm! zz
endfunction

noremap <silent><Leader>[ :call <SID>qfnavi(':lprevious',':llast')<CR>
noremap <silent><Leader>] :call <SID>qfnavi(':lnext',':lfirst')<CR>
noremap <silent><Leader>; :call <SID>qfnavi(':cprevious',':clast')<CR>
noremap <silent><Leader>' :call <SID>qfnavi(':cnext',':cfirst')<CR>

"toggles
nnoremap \] :vs 
nnoremap \[ :split 
nnoremap \|} :vs %:p:h/
nnoremap \|{ :split %:p:h/
nnoremap \p :tabnew 
nnoremap \|t :tabnew %:p:h/

nnoremap \t :TagbarToggle<CR>
nnoremap \u :UndotreeToggle<CR>
nnoremap \g :GitGutterToggle<CR>
nnoremap \r :RangerWorkingDirectory<CR>
nnoremap \l :call ToggleLocationList()<CR>
nnoremap \q :call ToggleQuickfixList()<CR>
nnoremap \b :buffers<CR>:b

"misc
xmap <BS> x
inoremap <C-U> <C-G>u<C-U>
nnoremap <Leader>i i_<ESC>r
nnoremap <Leader>o a_<ESC>r
let g:mucomplete#no_mappings=1
inoremap <silent> <plug>(MUcompleteBwdKey) <C-f>
inoremap <silent> <plug>(MUcompleteFwdKey) <C-g>
imap <C-f> <plug>(MUcompleteCycBwd)
imap <C-g> <plug>(MUcompleteCycFwd)
imap <C-j> <plug>(MUcompleteFwd)
imap <C-k> <plug>(MUcompleteBwd)

augroup bashfcgrp
    au!
    autocmd BufRead /tmp/bash-fc.*
                \ startinsert!
                \| noremap <buffer> <C-s> :wq<CR>
                \| inoremap <buffer> <C-s> :wq<CR>
augroup END

let g:peekaboo_ins_prefix= '<C-x>'
"imap <expr> <Leader><tab> mucomplete#extend_fwd("\<Leader>j")
let g:UltiSnipsExpandTrigger="<C-l>"
let g:UltiSnipsJumpForwardTrigger="<C-e>"
map <F5> :Limelight<CR>
map <F6> :Limelight!<CR>
map <F7> :Goyo<CR>
map <F8> :Goyo!<CR>

"colors
colorscheme monochrome
"PhoenixBlue

set showtabline=2
highlight TabLineSel ctermbg=230    ctermfg=235
highlight  TabLine      ctermbg=DARKGREEN ctermfg=DARKGREEN
hi TabLineFill ctermbg=DARKGREEN ctermfg=DARKGREEN
augroup TabLineColoring
   au!
    autocmd InsertEnter * highlight  TabLine        ctermbg=NONE ctermfg=DARKGREEN
    autocmd InsertEnter * highlight  TabLineFill    ctermbg=NONE ctermfg=DARKGREEN
    autocmd InsertLeave * highlight  TabLine        ctermbg=NONE ctermfg=DARKBLUE
    autocmd InsertLeave * highlight  TabLineFill    ctermbg=NONE ctermfg=DARKBLUE
augroup END

